\contentsline {section}{Sissejuhatus}{5}{section*.2}
\contentsline {section}{\numberline {1}\IeC {\"U}helerakendus}{6}{section.0.1}
\contentsline {subsection}{\numberline {1.1}Tehnoloogiate valik \IeC {\"u}heleherakenduse loomiseks}{7}{subsection.0.1.1}
\contentsline {section}{\numberline {2}Rakenduse idee}{10}{section.0.2}
\contentsline {section}{\numberline {3}Arenduse plaan}{11}{section.0.3}
\contentsline {subsection}{\numberline {3.1}Ruby on Rails valik}{12}{subsection.0.3.1}
\contentsline {section}{\numberline {4}Rakenduse loomine}{16}{section.0.4}
\contentsline {subsection}{\numberline {4.1}Andmete \IeC {\"u}htsele standardile viimine}{19}{subsection.0.4.1}
\contentsline {subsection}{\numberline {4.2}Probleemid rakenduse arendamisel}{20}{subsection.0.4.2}
\contentsline {subsection}{\numberline {4.3}Rakenduse k\IeC {\"a}ivitamine lokaalselt}{22}{subsection.0.4.3}
\contentsline {section}{Kokkuv\IeC {\~o}te}{24}{section*.3}
\contentsline {section}{Kasutatud kirjandus}{26}{section*.3}
\contentsline {section}{Summary}{27}{section*.5}
